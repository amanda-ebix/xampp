<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'loginit');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Vv!%+@ 3jE4Bo8YyA_Jh5Qu|x#7>P{_^H;w%EI.l8RX?`XjAFR]fR7*H %G/wWqN');
define('SECURE_AUTH_KEY',  'UJ>ULhV56 N_Ec5z|fy.O%`AKt.Uz}[A}gtRr`9*6SF(ou9Ky7xG0Xu-emLR>%!v');
define('LOGGED_IN_KEY',    'dfS>AClhPT>X5#K8[#|oAZiC@OS,wAqL^X+J%[I:BpgDSD`exi;;d?n>R2@7$@Y7');
define('NONCE_KEY',        'd>Ww`)AtO8GyMwW]@)O/&mlWRY2rRKCshBtbXGyD12.W- 2T.v[uy`oNrx&w-gim');
define('AUTH_SALT',        '^!yEc4A3:L^*d9&nLrOq51CQ4f4b&4O=wEx!&=%~L5QmP;<nRR;&Z]>uhc5!}+2l');
define('SECURE_AUTH_SALT', 'vW($y4L$|q74z pT+>|w<pxua&a1Z]=B!72 tT4}/2ewf9@$Jzrp5^YYG(V~>! v');
define('LOGGED_IN_SALT',   'Z)v}IUu{&8%2V.Bqy6K8A:g^s~gB=p1{^07z.G1Q]Gdn54(3V1s+z!jy16G{Ug/h');
define('NONCE_SALT',       '#yJlz4i,&?OgV/%%_S O&KA_b%]G:r#&tfJj(tIb3]r?v^^m6;&fss8KYexsV6w#');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
